django-dirtyfields (1.3.1-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.0, no changes needed.
  * Update standards version to 4.6.1, no changes needed.
  * Apply multi-arch hints. + python-django-dirtyfields-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 11:33:09 +0100

django-dirtyfields (1.3.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

  [ Christopher Hoskin ]
  * Bump compat from 12 to 13

  [ Debian Janitor ]
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Sun, 24 Apr 2022 01:51:07 -0400

django-dirtyfields (1.3.1-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * Convert git repository from git-dpm to gbp layout
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).

  [ Christopher Hoskin ]
  * Add .pytest_cache/ to clean

  [ Andrey Rahmatullin ]
  * Fix running tests with the current Django version.

 -- Andrey Rahmatullin <wrar@debian.org>  Thu, 15 Aug 2019 22:00:17 +0500

django-dirtyfields (1.3.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Christopher Hoskin ]
  * New upstream release (1.3.1)
  * Use secure URL for homepage
  * Bump debhelper compat from 9 to 11
  * Add upstream/metadata
  * Bump Standards Version from 4.1.1 to 4.1.3 (no change required)
  * Update copyright

 -- Christopher Hoskin <mans0954@debian.org>  Tue, 03 Apr 2018 22:29:04 +0100

django-dirtyfields (1.3-1) unstable; urgency=medium

  * Use GitHub for upstream source tarball
  * New upstream release (1.3)
  * Remove whitespace
  * Update Standards-Version from 3.9.8 to 4.1.1 (no change required)

 -- Christopher Hoskin <mans0954@debian.org>  Mon, 30 Oct 2017 23:25:37 +0000

django-dirtyfields (1.2.1-1) unstable; urgency=low

  * source package automatically created by stdeb 0.8.5 (Closes: #856888).

 -- Christopher Hoskin <mans0954@debian.org>  Sun, 05 Mar 2017 22:16:16 +0000
